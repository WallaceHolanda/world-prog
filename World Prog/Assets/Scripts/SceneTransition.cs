﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneTransition : MonoBehaviour {
	public string sceneToLoad;
	public Vector2 playerPosition;
	public VectorValue playerStorage;

	public void OnTriggerEnter2D(Collider2D other){
		if(other.CompareTag("Player") && !other.isTrigger){
			playerStorage.initialValue = playerPosition;
			SceneManager.LoadScene(sceneToLoad);

		}

	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
