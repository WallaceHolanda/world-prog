﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	public float speed;
	private Rigidbody2D myRigidbody;
	private Vector3 change;
	private Animator animator;
	public VectorValue startingPosition;


	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		myRigidbody = GetComponent<Rigidbody2D>();
		transform.position = startingPosition.initialValue;

	}
	
	// Update is called once per frame
	void Update () {
		change = Vector3.zero;
		change.x = Input.GetAxisRaw("Horizontal");
		change.y = Input.GetAxisRaw("Vertical");
		UpdateAnimationAndMove();
		//Debug.Log(change);
	}

    void UpdateAnimationAndMove(){
        if(change != Vector3.zero){ //Verifica se houve alguma mudança na posição
			MoveCaracter();
			animator.SetFloat("MoveX", change.x);
			animator.SetFloat("MoveY", change.y);
		    animator.SetBool("moving", true);
		} else {
			animator.SetBool("moving", false);
		}

	}


	void MoveCaracter(){
		myRigidbody.MovePosition(
			transform.position + change * speed * Time.deltaTime
		);

	}
}
