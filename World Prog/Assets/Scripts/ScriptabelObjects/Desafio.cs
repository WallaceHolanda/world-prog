﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Desafio : MonoBehaviour {
	//public GerenciadorSC moedas;

	public GameObject[] tesouro;
	static int indexSpawn = 0; //beforeIndex = 0;
	static int aux = 0;

	public GerenciadorSpawner aparecer;
	public DescriptionChallenges conteudos;

	public GameObject coinsBox;
	public Text coinsCount;
	public string[] valorMoeda;

	public GameObject dialogBox;
	public GameObject[] desafioImagem;
	public Text dialogText;

	public GameObject nextButton;
	public Text nextText;
	public GameObject beforeButton;
	public Text beforeText;

	public GameObject AButton;
	public Text AText;
	public GameObject BButton;
	public Text BText;
	public GameObject CButton;
	public Text CText;


	static int finalizaDesafio = 0;
	static bool playerInRange = false;
	static int i = 0;
	static int index = 0;
	static int newCount = 0;
	
	// Use this for initialization
	void Start () {
		HabilitaObjeto(coinsBox);
		coinsCount.text = "" + newCount;
		if(aux == 0){
			//Debug.Log("Entrei");
			indexSpawn = aux;
			aux = 1;
			//HabilitaObjeto(tesouro[indexSpawn]);
		} else { //testa se entrou na casa
			//indexSpawn = aux -1;
			DesabilitaObjeto(tesouro[0]);
			HabilitaObjeto(tesouro[indexSpawn]);
			//aparecer.HabilitaSpawn(); Verificar o spanw (ver a função)
		}


	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.Space) && playerInRange){
			if(dialogBox.activeInHierarchy){
				dialogBox.SetActive(false);
			} else{
				//dialogText.text = dialog;
				dialogBox.SetActive(true);
			}
		}

		/*if(spawnar == true){
			Debug.Log("Novo spawn");
			Spawnar();
			spawnar = false;
		}*/
	}

	public void DesabilitaObjeto(GameObject objeto){
		objeto.SetActive(false);
	}

	/*Mostrando o objeto da tela*/
	public void HabilitaObjeto(GameObject objeto){
		objeto.SetActive(true);
	}

	void realizaSpawn(){
		//Debug.Log("Entrei no spawn!");
		if(indexSpawn == 5){
			DesabilitaObjeto(tesouro[indexSpawn]);
			indexSpawn = 0;
			HabilitaObjeto(tesouro[indexSpawn]);
		}else{
			DesabilitaObjeto(tesouro[indexSpawn]);
			indexSpawn = indexSpawn +1;
			HabilitaObjeto(tesouro[indexSpawn]);
		}
	}


	void CarregaConteudo(){
		dialogText.text = conteudos.dialog[i];
		AText.text = conteudos.AText[i];
		BText.text = conteudos.BText[i];
		CText.text = conteudos.CText[i];
		//HabilitaObjeto(dialogText);
		HabilitaObjeto(AButton);
		HabilitaObjeto(BButton);
		HabilitaObjeto(CButton);
	}

	void DesabilitaBotoes(){
		DesabilitaObjeto(desafioImagem[i]);
		DesabilitaObjeto(nextButton);
		DesabilitaObjeto(beforeButton);
		DesabilitaObjeto(AButton);
		DesabilitaObjeto(BButton);
		DesabilitaObjeto(CButton);
	}

	public void ApresentaProximoDialogo(){
		if(finalizaDesafio == 1){ // Utilizando o mesmo botão para fechar a caixa de diálogo;
			DesabilitaObjeto(dialogBox);
			DesabilitaObjeto(nextButton);
			playerInRange = false;
			finalizaDesafio = 0;
		} else {
			
			index = index + 1;
			dialogText.text = "";
			HabilitaObjeto(desafioImagem[i]);
			HabilitaObjeto(dialogBox);
			DesabilitaObjeto(nextButton);
			HabilitaObjeto(beforeButton);
		}
		/*}else{
			DesabilitaObjeto(dialogBox);
			playerInRange = false; //Alan saiu da área de interação
		}*/
	}

	//verificar se termina no anterior
	public void ApresentaDialogoAnterior(){
		index = index - 1;
		dialogText.text =  conteudos.dialog[i];
		DesabilitaObjeto(desafioImagem[i]);
		DesabilitaObjeto(beforeButton);
		HabilitaObjeto(dialogBox);
		HabilitaObjeto(nextButton);
	}

	public void VerificaPrimeira(int x){
		if(x == conteudos.answer[i]){ //conteudos.answer[i]
			DesabilitaBotoes();
			HabilitaObjeto(nextButton);
			nextText.text = "Fechar";
			coinsCount.text = "" + (i+1);
			if(newCount+1 == 6){
				//Debug.Log("Finalizei o desafio!\n");
				DesabilitaObjeto(tesouro[indexSpawn]);
				dialogText.text = "A resposta está correta! Você ganhou mais uma moeda!\nCom isso, você conseguiu as seis modeas necessárias para finalizar o primeiro mundo do World Prog!";
			}else{
				//aparecer.HabilitaSpawn();
				realizaSpawn();
				dialogText.text = "A resposta está correta!\nVocê acaba de ganhar mais uma moeda!";
				conteudos.NewChallenge();
				finalizaDesafio = 1;
				newCount++;
			}
		
		}else{
			//aparecer.HabilitaSpawn();
			realizaSpawn();
			dialogText.text = "A resposta está incorreta!\n";
			DesabilitaBotoes();
			HabilitaObjeto(nextButton);
			nextText.text = "Fechar";
			finalizaDesafio = 1;
		}
	}

	//Debug.Log("Vai dar certo!");

	private void OnTriggerEnter2D(Collider2D other){
		if(other.CompareTag("Player")){
			i = conteudos.GetIndex();
			
			dialogText.text = conteudos.dialog[i];
			AText.text = conteudos.AText[i];
			BText.text = conteudos.BText[i];
			CText.text = conteudos.CText[i];
			//HabilitaObjeto(dialogText);
			HabilitaObjeto(AButton);
			HabilitaObjeto(BButton);
			HabilitaObjeto(CButton);

			HabilitaObjeto(dialogBox);
			nextText.text = "Próximo";
			HabilitaObjeto(nextButton);
			DesabilitaObjeto(beforeButton);
			playerInRange = true;
			//conteudos.NewChallenge();
		}
	}

	private void OnTriggerExit2D(Collider2D other){
		if(other.CompareTag("Player")){
			playerInRange = false;
			DesabilitaBotoes();
			DesabilitaObjeto(dialogBox);
			DesabilitaObjeto(desafioImagem[i]);
		}
	}

}
