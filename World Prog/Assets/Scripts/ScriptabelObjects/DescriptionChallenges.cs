﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class DescriptionChallenges : ScriptableObject  {
	[TextArea(1, 4)]
	public string[] dialog;
	public string[] AText;
	public string[] BText;
	public string[] CText;	
	public int[] answer;
	static int i = 0;
	
	public int GetIndex(){
		return i;
	}

	public void NewChallenge(){
		i = i + 1;
	}
	
}
