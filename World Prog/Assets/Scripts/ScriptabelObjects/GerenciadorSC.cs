﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class GerenciadorSpawner : ScriptableObject {
	public static int spawn = 1;
	
	public int GetSpawn(){
		return spawn;
	}

	public void HabilitaSpawn(){
		spawn = 1;
	}

	public void DesabilitaSpawn(){
		spawn = 0;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
