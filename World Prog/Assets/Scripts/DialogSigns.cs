﻿using UnityEngine.UI;
//using TMPro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DialogSigns : MonoBehaviour {
	public GameObject dialogBox;
	public Text dialogText;

	[TextArea(1, 4)]
	public string dialog;

	[SerializeField]
	public GameObject imagemPeca;

	public bool playerInRange;

	/*Escondendo o objeto da tela*/
	public void DesabilitaObjeto(GameObject objeto){
		objeto.SetActive(false);
		//nextText.enabled = false; Não há necessidade, pois a hierarquia do botão é maior
	}

	/*Mostrando o objeto (botão, caixa de diálogo) da tela*/
	public void HabilitaObjeto(GameObject objeto){
		objeto.SetActive(true);
		//nextText.enabled = true; Não há necessidade, pois a hierarquia do botão é maior 
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.Space) && playerInRange){
			if(dialogBox.activeInHierarchy){
				DesabilitaObjeto(dialogBox);
			} else{
				//dialogText.text = dialog;
				HabilitaObjeto(dialogBox);
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D other){
		if(other.CompareTag("Player")){
			//imagemPeca.enabled = true;
			HabilitaObjeto(imagemPeca);
			dialogText.text = dialog;
			HabilitaObjeto(dialogBox);
			playerInRange = true;
		}
	}

	private void OnTriggerExit2D(Collider2D other){
		if(other.CompareTag("Player")){
			playerInRange = false;
			DesabilitaObjeto(dialogBox);
			DesabilitaObjeto(imagemPeca);
		}
	}



}
