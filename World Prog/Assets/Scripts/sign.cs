﻿using UnityEngine.UI;
//using TMPro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sign : MonoBehaviour {
	public GameObject dialogBox;
	public Text dialogText;

	[TextArea(1, 4)]
	public string[] dialog;
	public bool playerInRange;
	public int[] checkImage;
	public Image imagemAlan;
	public Image imagemTree;
	public static int i = 0;
	public GameObject nextButton;
	public Text nextText;
	public GameObject beforeButton;
	public Text beforeText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.Space) && playerInRange){
			if(dialogBox.activeInHierarchy){
				dialogBox.SetActive(false);
			} else{
				//dialogText.text = dialog;
				dialogBox.SetActive(true);
			}
		}
	}


	/*Escondendo o objeto da tela*/
	public void DesabilitaObjeto(GameObject objeto){
		objeto.SetActive(false);
		//nextText.enabled = false; Não há necessidade, pois a hierarquia do botão é maior
	}

	/*Mostrando o objeto da tela*/
	public void HabilitaObjeto(GameObject objeto){
		objeto.SetActive(true);
		//nextText.enabled = true; Não há necessidade, pois a hierarquia do botão é maior
	}

	/* */
	public void InverteImagem(Image img1, Image img2, int counter){
		if(counter == 1){
			img1.enabled = true;
			img2.enabled = false;
		}else{
			img1.enabled = false;
			img2.enabled = true;
		}
	}

	public void ApresentaProximoDialogo(){
		int tam = dialog.Length;
		//Debug.Log(tam);
		i = i + 1;
		if (i == tam-1){
			InverteImagem(imagemTree, imagemAlan, checkImage[i]);
			dialogText.text = dialog[i];
			//DesabilitaObjeto(nextButton);
			nextText.text = "Sair";
			HabilitaObjeto(dialogBox);
			playerInRange = true;
		}else if(i < tam){
			InverteImagem(imagemTree, imagemAlan, checkImage[i]);
			HabilitaObjeto(beforeButton);
			dialogText.text = dialog[i];
			HabilitaObjeto(dialogBox);
			playerInRange = true;
		}else{
			DesabilitaObjeto(dialogBox);
			playerInRange = false; //Alan saiu da área de interação
		}
	}

	public void ApresentaDialogoAnterior(){
		i = i - 1;
		if(i > 0){
			InverteImagem(imagemTree, imagemAlan, checkImage[i]);
			nextText.text = "Próximo";
			dialogText.text = dialog[i];
			HabilitaObjeto(nextButton);
			HabilitaObjeto(dialogBox);
			playerInRange = true;
		}else if(i == 0){
			InverteImagem(imagemTree, imagemAlan, checkImage[i]);
			dialogText.text = dialog[i];
			DesabilitaObjeto(beforeButton);
			HabilitaObjeto(dialogBox);
			playerInRange = true;
		} else{
			DesabilitaObjeto(dialogBox);
			playerInRange = false; //Alan saiu da área de interação
		}

	}

	//Debug.Log("Vai dar certo!");

	private void OnTriggerEnter2D(Collider2D other){
		if(other.CompareTag("Player")){
			i = 0; // Iniciando do primeiro diálogo
			nextText.text = "Próximo";
			InverteImagem(imagemTree, imagemAlan, checkImage[i]);
			DesabilitaObjeto(beforeButton);
			dialogText.text = dialog[i];
			HabilitaObjeto(dialogBox);
			playerInRange = true;
		}
	}

	private void OnTriggerExit2D(Collider2D other){
		if(other.CompareTag("Player")){
			playerInRange = false;
			DesabilitaObjeto(dialogBox);
		}
	}

}
