﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoviment : MonoBehaviour {
	public Transform target;
	public float smoothing;

	//Limitar a câmera para o cenário
	public Vector2 maxPosition;
	public Vector2 minPosition;

	void Start () {
		transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void LateUpdate () {
		
		if(transform.position != target.position){
			//O alvo terá as coordenadas de x e y, acompanhado pela transformação em z
			Vector3 targetPosition = new Vector3(target.position.x, target.position.y, transform.position.z);

			targetPosition.x = Mathf.Clamp(targetPosition.x, 
											minPosition.x,
											maxPosition.x);

			targetPosition.y = Mathf.Clamp(targetPosition.y, 
											minPosition.y,
											maxPosition.y);

			transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing);

		}

	}
	
}
