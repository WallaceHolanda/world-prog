﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartDialog : MonoBehaviour {
	public GameObject dialogBox;
	public Text dialogText;
	[TextArea(1, 4)]
	public string[] dialog;

	public GameObject nextButton;
	public Text nextText;
	public GameObject beforeButton;
	public Text beforeText;

	static int tam = 0;
	static int i = 0;
	static int controler = 0;

	// Use this for initialization
	void Start () {
		if(controler == 0){
			dialogText.text = dialog[i];
			controler = 1;
		}else{
			DesabilitaObjeto(dialogBox);
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void HabilitaObjeto(GameObject objeto){
		objeto.SetActive(true);
	}

	void DesabilitaObjeto(GameObject objeto){
		objeto.SetActive(false);
	}


	/* 
	** Esta função irá carregar o contéudo do próximo texto
	** Esta função será acionada assim que o usuário clicar no botão "próximo"
	*/
	public void ApresentaProximoDialogo(){
		tam = dialog.Length;
		i = i + 1;
		if(i == tam){
			DesabilitaObjeto(dialogBox);
		}else if (i == tam-1){
			dialogText.text = dialog[i];
			//DesabilitaObjeto(nextButton);
			nextText.text = "Sair";
			HabilitaObjeto(dialogBox);
		}else if(i < tam){
			HabilitaObjeto(beforeButton);
			dialogText.text = dialog[i];
			//HabilitaObjeto(dialogBox);
		}		
	}


	/* 
	** Esta função irá carregar o contéudo do texto anterior
	** Esta função será acionada assim que o usuário clicar no botão "anterior"
	*/
	public void ApresentaDialogoAnterior(){
		i = i - 1;
		if(i == 0){
			dialogText.text = dialog[i];
			DesabilitaObjeto(beforeButton);
			HabilitaObjeto(dialogBox);
		}else { //if(i > 0)
			nextText.text = "Próximo";
			dialogText.text = dialog[i];
			HabilitaObjeto(nextButton);
			HabilitaObjeto(dialogBox);
		}
	}

}
